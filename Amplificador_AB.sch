EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Amplificador Clase AB"
Date "2019-12-07"
Rev "0"
Comp ""
Comment1 "Alumnos: Di Sanzo, German y Píccolo, Sergio"
Comment2 "Profesores: Ing Aparicio Hugo y Ing Pohl Alejandro"
Comment3 "Electrónica Aplicada II"
Comment4 "UTN FRH"
$EndDescr
$Comp
L Device:R_US R1
U 1 1 5DEBDD10
P 2200 2850
F 0 "R1" H 2268 2896 50  0000 L CNN
F 1 "68K" H 2268 2805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2240 2840 50  0001 C CNN
F 3 "~" H 2200 2850 50  0001 C CNN
	1    2200 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R2
U 1 1 5DEBE5F0
P 2200 3950
F 0 "R2" H 2268 3996 50  0000 L CNN
F 1 "39K" H 2268 3905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2240 3940 50  0001 C CNN
F 3 "~" H 2200 3950 50  0001 C CNN
	1    2200 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R5
U 1 1 5DEBEA95
P 2200 5500
F 0 "R5" H 2268 5546 50  0000 L CNN
F 1 "56" H 2268 5455 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2240 5490 50  0001 C CNN
F 3 "~" H 2200 5500 50  0001 C CNN
	1    2200 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R6
U 1 1 5DEBECE6
P 3700 3400
F 0 "R6" V 3495 3400 50  0000 C CNN
F 1 "1.2K" V 3586 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3740 3390 50  0001 C CNN
F 3 "~" H 3700 3400 50  0001 C CNN
	1    3700 3400
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R3
U 1 1 5DEBFCCA
P 3150 5450
F 0 "R3" H 3218 5496 50  0000 L CNN
F 1 "1.8K" H 3218 5405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3190 5440 50  0001 C CNN
F 3 "~" H 3150 5450 50  0001 C CNN
	1    3150 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US RC2-2
U 1 1 5DEC22A9
P 4500 1700
F 0 "RC2-2" H 4568 1746 50  0000 L CNN
F 1 "120" H 4568 1655 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4540 1690 50  0001 C CNN
F 3 "~" H 4500 1700 50  0001 C CNN
	1    4500 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R10
U 1 1 5DECAA72
P 5700 3050
F 0 "R10" H 5768 3096 50  0000 L CNN
F 1 "0.39" H 5768 3005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5740 3040 50  0001 C CNN
F 3 "~" H 5700 3050 50  0001 C CNN
	1    5700 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R11
U 1 1 5DECAA7C
P 5700 4000
F 0 "R11" H 5768 4046 50  0000 L CNN
F 1 "0.39" H 5768 3955 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5740 3990 50  0001 C CNN
F 3 "~" H 5700 4000 50  0001 C CNN
	1    5700 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:D D1
U 1 1 5DED39C6
P 4500 3000
F 0 "D1" V 4546 2921 50  0000 R CNN
F 1 "1N4148" V 4455 2921 50  0000 R CNN
F 2 "Diode_THT:D_DO-15_P12.70mm_Horizontal" H 4500 3000 50  0001 C CNN
F 3 "~" H 4500 3000 50  0001 C CNN
	1    4500 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D2
U 1 1 5DED434C
P 4500 3800
F 0 "D2" V 4546 3721 50  0000 R CNN
F 1 "1N4148" V 4455 3721 50  0000 R CNN
F 2 "Diode_THT:D_DO-15_P12.70mm_Horizontal" H 4500 3800 50  0001 C CNN
F 3 "~" H 4500 3800 50  0001 C CNN
	1    4500 3800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_POT_US R7
U 1 1 5DED53D1
P 4500 4400
F 0 "R7" H 4432 4446 50  0000 R CNN
F 1 "27" H 4432 4355 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Vishay_148-149_Single_Horizontal" H 4500 4400 50  0001 C CNN
F 3 "~" H 4500 4400 50  0001 C CNN
	1    4500 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5DEE442E
P 2900 3200
F 0 "C2" V 3155 3200 50  0000 C CNN
F 1 "47uF" V 3064 3200 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 2938 3050 50  0001 C CNN
F 3 "~" H 2900 3200 50  0001 C CNN
	1    2900 3200
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP C1
U 1 1 5DEE55C0
P 5200 2950
F 0 "C1" H 5318 2996 50  0000 L CNN
F 1 "100uF" H 5318 2905 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 5238 2800 50  0001 C CNN
F 3 "~" H 5200 2950 50  0001 C CNN
	1    5200 2950
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC559 Q1
U 1 1 5DEEEBA4
P 3050 3700
F 0 "Q1" H 3241 3654 50  0000 L CNN
F 1 "BC559" H 3241 3745 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 3250 3625 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC557.pdf" H 3050 3700 50  0001 L CNN
	1    3050 3700
	1    0    0    1   
$EndComp
$Comp
L Transistor_BJT:BC337 Q2
U 1 1 5DEF01F1
P 4400 5050
F 0 "Q2" H 4591 5096 50  0000 L CNN
F 1 "BC337" H 4591 5005 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 4600 4975 50  0001 L CIN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/bc337.pdf" H 4400 5050 50  0001 L CNN
	1    4400 5050
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BD437 Q3
U 1 1 5DEF1D89
P 5600 2600
F 0 "Q3" H 5792 2646 50  0000 L CNN
F 1 "BD437" H 5792 2555 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-126-3_Vertical" H 5800 2525 50  0001 L CIN
F 3 "http://www.cdil.com/datasheets/bd433_42.pdf" H 5600 2600 50  0001 L CNN
	1    5600 2600
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BD438 Q4
U 1 1 5DEF3000
P 5600 4700
F 0 "Q4" H 5792 4654 50  0000 L CNN
F 1 "BD438" H 5792 4745 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-126-3_Vertical" H 5800 4625 50  0001 L CIN
F 3 "http://www.cdil.com/datasheets/bd433_42.pdf" H 5600 4700 50  0001 L CNN
	1    5600 4700
	1    0    0    1   
$EndComp
Wire Wire Line
	2200 4100 2200 4250
Wire Wire Line
	2850 3700 2200 3700
Wire Wire Line
	2200 3700 2200 3800
$Comp
L Device:CP C3
U 1 1 5DEE6EEB
P 1700 3700
F 0 "C3" V 1445 3700 50  0000 C CNN
F 1 "1uF" V 1536 3700 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 1738 3550 50  0001 C CNN
F 3 "~" H 1700 3700 50  0001 C CNN
	1    1700 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 3400 3150 3400
Wire Wire Line
	3150 3400 3150 3500
Wire Wire Line
	2200 3000 2200 3700
Connection ~ 2200 3700
Wire Wire Line
	1850 3700 2200 3700
$Comp
L Connector:Screw_Terminal_01x02 CN1
U 1 1 5DF27FE4
P 900 4250
F 0 "CN1" H 818 3925 50  0000 C CNN
F 1 "Vin" H 818 4016 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 900 4250 50  0001 C CNN
F 3 "~" H 900 4250 50  0001 C CNN
	1    900  4250
	-1   0    0    1   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 CN3
U 1 1 5DF30EE0
P 6800 2800
F 0 "CN3" H 6880 2792 50  0000 L CNN
F 1 "Vout" H 6880 2701 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 6800 2800 50  0001 C CNN
F 3 "~" H 6800 2800 50  0001 C CNN
	1    6800 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3900 3150 5050
Wire Wire Line
	1100 4250 1100 5800
Wire Wire Line
	1100 5800 2200 5800
Wire Wire Line
	4500 5800 4500 5250
Wire Wire Line
	3150 5600 3150 5800
Connection ~ 3150 5800
Wire Wire Line
	3150 5800 4500 5800
Wire Wire Line
	2200 5650 2200 5800
Connection ~ 2200 5800
Wire Wire Line
	2200 5800 3150 5800
Wire Wire Line
	3050 3200 3150 3200
Wire Wire Line
	3150 3200 3150 3400
Connection ~ 3150 3400
Wire Wire Line
	1550 3700 1100 3700
Wire Wire Line
	1100 3700 1100 4150
Wire Wire Line
	4500 3650 4500 3150
Wire Wire Line
	4500 2450 4500 2600
Wire Wire Line
	4500 1250 2200 1250
Wire Wire Line
	2200 1250 2200 2700
Wire Wire Line
	4500 1550 4500 1250
Wire Wire Line
	4500 1250 5700 1250
Wire Wire Line
	7700 1250 7700 3000
Connection ~ 4500 1250
Connection ~ 4500 5800
$Comp
L Connector:Screw_Terminal_01x03 CN2
U 1 1 5DF301C9
P 7900 3100
F 0 "CN2" H 7980 3142 50  0000 L CNN
F 1 "VCC" H 7980 3051 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-3_P5.08mm" H 7900 3100 50  0001 C CNN
F 3 "~" H 7900 3100 50  0001 C CNN
	1    7900 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 5800 5700 5800
Wire Wire Line
	7700 3200 7700 5800
Wire Wire Line
	5400 4700 4500 4700
Wire Wire Line
	4500 4700 4500 4850
Wire Wire Line
	5700 4900 5700 5800
Connection ~ 5700 5800
Wire Wire Line
	5700 5800 7700 5800
Wire Wire Line
	5700 3200 5700 3400
Wire Wire Line
	5700 4150 5700 4500
Wire Wire Line
	3850 3400 5200 3400
Connection ~ 5700 3400
Wire Wire Line
	5700 3400 5700 3850
$Comp
L Device:R_US RC2
U 1 1 5DEC9B17
P 4500 2300
F 0 "RC2" H 4568 2346 50  0000 L CNN
F 1 "390" H 4568 2255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4540 2290 50  0001 C CNN
F 3 "~" H 4500 2300 50  0001 C CNN
	1    4500 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 2600 4500 2600
Connection ~ 4500 2600
Wire Wire Line
	4500 2600 4500 2850
Wire Wire Line
	5700 2400 5700 1250
Connection ~ 5700 1250
Wire Wire Line
	5700 1250 7700 1250
Wire Wire Line
	5700 2800 5700 2900
Wire Wire Line
	4500 1850 4500 2000
Wire Wire Line
	5200 2800 5200 2000
Wire Wire Line
	5200 2000 4500 2000
Connection ~ 4500 2000
Wire Wire Line
	4500 2000 4500 2150
Wire Wire Line
	5200 3100 5200 3400
Connection ~ 5200 3400
Wire Wire Line
	5200 3400 5700 3400
Wire Wire Line
	6600 3100 7700 3100
Wire Wire Line
	6600 2900 6600 3100
Wire Wire Line
	6600 2800 6050 2800
Wire Wire Line
	6050 2800 6050 3400
Wire Wire Line
	6050 3400 5700 3400
Wire Wire Line
	4500 3950 4500 4100
Wire Wire Line
	4650 4400 4850 4400
Wire Wire Line
	4850 4400 4850 4100
Wire Wire Line
	4850 4100 4500 4100
Connection ~ 4500 4100
Wire Wire Line
	4500 4100 4500 4250
Wire Wire Line
	4500 4550 4500 4700
Connection ~ 4500 4700
$Comp
L Device:R_POT_US R4
U 1 1 5DED4BBB
P 2200 4650
F 0 "R4" H 2132 4696 50  0000 R CNN
F 1 "10K" H 2132 4605 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Vishay_148-149_Single_Horizontal" H 2200 4650 50  0001 C CNN
F 3 "~" H 2200 4650 50  0001 C CNN
	1    2200 4650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2200 4800 2200 5100
Wire Wire Line
	2050 4650 1800 4650
Wire Wire Line
	1800 4650 1800 4250
Wire Wire Line
	1800 4250 2200 4250
Connection ~ 2200 4250
Wire Wire Line
	2200 4250 2200 4500
Wire Wire Line
	2200 5100 2600 5100
Wire Wire Line
	2600 5100 2600 3200
Wire Wire Line
	2600 3200 2750 3200
Connection ~ 2200 5100
Wire Wire Line
	2200 5100 2200 5350
Wire Wire Line
	4200 5050 4100 5050
Connection ~ 3150 5050
Wire Wire Line
	3150 5050 3150 5300
$Comp
L Device:CP C4
U 1 1 5DF481BE
P 2900 2800
F 0 "C4" V 3155 2800 50  0000 C CNN
F 1 "22uF" V 3064 2800 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 2938 2650 50  0001 C CNN
F 3 "~" H 2900 2800 50  0001 C CNN
	1    2900 2800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2750 2800 2750 3200
Connection ~ 2750 3200
Wire Wire Line
	3050 2800 3050 3200
Connection ~ 3050 3200
$Comp
L Device:CP C5
U 1 1 5DF4C9BE
P 4250 4700
F 0 "C5" V 4505 4700 50  0000 C CNN
F 1 "4.7nF" V 4414 4700 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 4288 4550 50  0001 C CNN
F 3 "~" H 4250 4700 50  0001 C CNN
	1    4250 4700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4400 4700 4500 4700
Wire Wire Line
	4100 4700 4100 5050
Connection ~ 4100 5050
Wire Wire Line
	4100 5050 3150 5050
$EndSCHEMATC
